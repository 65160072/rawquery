import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Invoice } from './Invoice';
import { Employee } from './Employee'; // Assuming there's an Employee entity

@Entity('customers')
export class Customer {
  @PrimaryGeneratedColumn()
  CustomerId: number;

  @Column({ type: 'nvarchar', length: 40 })
  FirstName: string;

  @Column({ type: 'nvarchar', length: 20 })
  LastName: string;

  @Column({ type: 'nvarchar', length: 80, nullable: true })
  Company: string;

  @Column({ type: 'nvarchar', length: 70, nullable: true })
  Address: string;

  @Column({ type: 'nvarchar', length: 40, nullable: true })
  City: string;

  @Column({ type: 'nvarchar', length: 40, nullable: true })
  State: string;

  @Column({ type: 'nvarchar', length: 40, nullable: true })
  Country: string;

  @Column({ type: 'nvarchar', length: 10, nullable: true })
  PostalCode: string;

  @Column({ type: 'nvarchar', length: 24, nullable: true })
  Phone: string;

  @Column({ type: 'nvarchar', length: 24, nullable: true })
  Fax: string;

  @Column({ type: 'nvarchar', length: 60 })
  Email: string;

  @Column({ nullable: true })
  SupportRepId: number;

  @OneToMany(() => Invoice, (invoice) => invoice.customer)
  invoices: Invoice[];

  @ManyToOne(() => Employee, (employee) => employee.customers)
  @JoinColumn({ name: 'SupportRepId' })
  supportRepresentative: Employee;
}
