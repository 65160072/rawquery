import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Album } from './Album';
import { Genre } from './Genre';
import { MediaType } from './MediaType';
import { InvoiceItem } from './InvoiceItem';
import { PlaylistTrack } from './PlaylistTrack';

@Entity('tracks')
export class Track {
  @PrimaryGeneratedColumn()
  TrackId: number;

  @Column({ type: 'nvarchar', length: 200 })
  Name: string;

  @Column({ nullable: true })
  AlbumId: number;

  @Column()
  MediaTypeId: number;

  @Column({ nullable: true })
  GenreId: number;

  // Other columns omitted for brevity

  @ManyToOne(() => Album, (album) => album.tracks)
  @JoinColumn({ name: 'AlbumId' })
  album: Album;

  @ManyToOne(() => Genre, (genre) => genre.tracks)
  @JoinColumn({ name: 'GenreId' })
  genre: Genre;

  @ManyToOne(() => MediaType, (mediaType) => mediaType.tracks)
  @JoinColumn({ name: 'MediaTypeId' })
  mediaType: MediaType;

  @OneToMany(() => InvoiceItem, (invoiceItem) => invoiceItem.track)
  invoiceItems: InvoiceItem[];

  @OneToMany(() => PlaylistTrack, (playlistTrack) => playlistTrack.track)
  playlistTracks: PlaylistTrack[];
}
