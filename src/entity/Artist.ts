import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Album } from './Album';

@Entity('artists')
export class Artist {
  @PrimaryGeneratedColumn()
  ArtistId: number;

  @Column({ type: 'nvarchar', length: 120, nullable: true })
  Name: string;

  @OneToMany(() => Album, (album) => album.artist)
  albums: Album[];
}
