import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Customer } from './Customer';
import { InvoiceItem } from './InvoiceItem';

@Entity('invoices')
export class Invoice {
  @PrimaryGeneratedColumn()
  InvoiceId: number;

  @Column()
  CustomerId: number;

  @Column('datetime')
  InvoiceDate: Date;

  // Billing details omitted for brevity

  @Column('numeric', { precision: 10, scale: 2 })
  Total: number;

  @ManyToOne(() => Customer, (customer) => customer.invoices)
  @JoinColumn({ name: 'CustomerId' })
  customer: Customer;

  @OneToMany(() => InvoiceItem, (invoiceItem) => invoiceItem.invoice)
  invoiceItems: InvoiceItem[];
}
