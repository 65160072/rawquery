import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Track } from './Track';

@Entity('media_types')
export class MediaType {
  @PrimaryGeneratedColumn()
  MediaTypeId: number;

  @Column({ type: 'nvarchar', length: 120, nullable: true })
  Name: string;

  @OneToMany(() => Track, (track) => track.mediaType)
  tracks: Track[];
}
