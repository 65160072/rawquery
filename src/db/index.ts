import { InvoiceItem } from './../entity/InvoiceItem';
import { AppDataSource } from './data-source';

AppDataSource.initialize()
  .then(async () => {
    const invoiceItemRepository = AppDataSource.getRepository(InvoiceItem);
    const query = await invoiceItemRepository
      .createQueryBuilder('it')
      .leftJoinAndSelect('it.track', 't') // สมมติว่า 'track' คือ relation ใน entity InvoiceItem ไปยัง Track
      .leftJoinAndSelect('t.album', 'a') // สมมติว่า 'album' คือ relation ใน entity Track ไปยัง Album
      .select([
        'a.ArtistId as ArtistId',
        'a.Title as Title',
        't.Name as Name',
        'SUM(it.UnitPrice) AS Price',
      ])
      .groupBy('t.AlbumId')
      .orderBy('Price', 'DESC')
      .getRawMany();
    console.log(query);
    const result =
      await invoiceItemRepository.query(`SELECT a.ArtistId, a.Title, t.Name, SUM(UnitPrice) as Price 
      FROM invoice_items it LEFT JOIN tracks t ON  it.TrackId = t.TrackId
    LEFT JOIN albums a ON a.AlbumId = t.AlbumId
    GROUP BY t.AlbumId
    ORDER BY Price DESC`);
    console.log(result);
  })

  .catch((error) => console.log(error));
